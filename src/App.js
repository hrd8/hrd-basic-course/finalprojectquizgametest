import './App.css';
import {Button, Col, Container, Row} from 'react-bootstrap'
import {useEffect, useRef, useState} from "react";
import Prompt from "./Prompt";
function App() {
    const [{question,correctAnswer,options},setMcq] = useState({
        question:"10 + 5 = ?",
        correctAnswer: "15",
        options:["105","15","150","60"],
    })
    const [isCorrect,setIsCorrect] = useState(false);
    const[isAnswer,setIsAnswer] = useState(false);
    const isFirstRun = useRef(true);
    const [temp,setTemp] = useState(false);
    useEffect(()=>{
        if(!isFirstRun.current){
            setTemp(true);
        }else {
            isFirstRun.current = false;
        }
    },[isAnswer,isFirstRun,isCorrect])
    let onHandleAnswer = (option) =>{
        setIsAnswer(!isAnswer);
        if (option===correctAnswer){
            setIsCorrect(true);
        }else {
            setIsCorrect(false);
        }
    }
  return (
    <div>
      <Container fluid>
          <Row className="space-evenly">
            <div className="game flex-space-evenly">
                    <h1>{question}</h1>
                    {temp ? <Prompt isCorrect={isCorrect} setTemp={setTemp}/>:""}
                    <Row>
                        {
                            options.map((opt,idx)=>(
                                <Col key={idx}>
                                    <Button className="answer-button bold-text" onClick={()=>onHandleAnswer(opt)}>{opt}</Button>
                                </Col>
                            ))
                        }
                    </Row>
            </div>
  </Row>
</Container>
    </div>
  );
}

export default App;
