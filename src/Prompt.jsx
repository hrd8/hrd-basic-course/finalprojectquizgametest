import React, {useEffect, useRef, useState} from 'react';
import correct from './check.svg';
import inCorrect from './cancel-mark.svg';

const Prompt = ({isCorrect,setTemp}) => {
    const [test,setTest] = useState(false);
    const isFirstRun =  useRef(true);
    useEffect(()=>{
        if (!isFirstRun.current){
            setTemp(false);
        }else{
            isFirstRun.current = false;
        }

    },[test,setTemp])
    let onHandleTest=()=>{
        setTest(!test)
    }

    return (
        <div>
            <div className="postAnswer">
                <div className="afterAnswer" id="afterAnswer">
                    <button className="close" onClick={onHandleTest}>✖</button>
                    <img src={isCorrect ? correct:inCorrect} alt="img"/>
                    <p>{isCorrect ? "That is the Correct Answer!":"\"That is not the Correct Answer!\""}</p>
                    <button className="accept" onClick={onHandleTest}>{isCorrect ? "Again?":"Try Again!"}</button>
                </div>
            </div>
        </div>
    );
};

export default Prompt;
